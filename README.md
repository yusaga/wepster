# wepster

Сборщик статичных сайтов на базе Gulp + планы для Wordpress

## TODO
 * Добавить описание процесса всей сборки
 * Добавить proxy и слежение за изменениями для темы  Wordpress

## Установка

```
git clone https://yunusgaziev@bitbucket.org/yunusgaziev/wepster.git && cd wepster && git pull --all && npm i . -g && npm link
```

## Как работает
создаем папку проекта, открываем её (папку) в терминале, запускаем сборку командой `wepster`

## разработка
все необходимо производить строго в ветке `develop`

## Сборка JS

структура иходников файлов скриптов

 * `wepster/_blocks/**/*.js` для блоков (подключаются в файлы)
 * `wepster/js/vendors` для `jquery`, `modernizr` и т.п. (просто перекладываются)
 * `wepster/js/plugins/**/*.js` для плагинов (обжимаются в `plugins.min.js`)
 * `wester/js/*.js` каждый файл обрабатывается отдельно

все файлы обрабатываюся, перекладываются в папку выше `wepster`, по умолчанию в `static`
```
.
└── static
    ├── js
    │   └── vendors
    │       ├── jquery.min.js
    │       └── modernizr.min.js
    ├── plugins.min.js
    ├── ga.js
    └── app.js
```
### подключение скриптов в скрипты :)
подключение файлов обеспечено плагином [gulp-include#include-directives](https://www.npmjs.com/package/gulp-include#include-directives)
#### подключаем все блоки в один файл
```
// например: в wepster/js/blocks.js
//=require ../_blocks/**/*.js
```
#### подключаем конкретный блок в файл
```
// например: в wepster/js/blocks.native.js
//=require ../_blocks/**/scroll-top.js
```
### примечание
т.к. под капотом [gulp-include#features](https://www.npmjs.com/package/gulp-include#features) работает [glob](https://www.npmjs.com/package/glob), и если предположить, что часть скриптов блоков, не нуждается в `jquery`, и должны сработать до события `domready` или им просто нет смысла ждать загрузки `jquery`, можно разделить их простыми и понтяными расширениями

 * `.jquery.js` - для зависимых от `jquery`
 * `.vanilla.js` - для независимых скриптов

```
.
└── wepster
    ├── _blocks
    │   ├── header
    │   │   ├── header.hbs
    │   │   └── header.jquery.js
    │   ├── main
    │   │   ├── main.hbs
    │   │   └── main.vanilla.js
    │   └── footer
    │       ├── footer.hbs
    │       └── footer.jquery.js
    └── js
        ├── blocks.jquery.js
        └── blocks.vanilla.js

// в blocks.jquery.js
//=require ../_blocks/**/*.jquery.js

// в blocks.vanilla.js
//=require ../_blocks/**/*.vanilla.js
```
### eslint
все скрипты, кроме вендорных и плагинов, обрабатываются `eslint`, настроить его можно в файле `wepster/.eslintrc`, установлены конфиги для `wordpress` и `jquery`, подключен `wordpress` конфиг
