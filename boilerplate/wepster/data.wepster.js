module.exports = {
    app      : {
        lang      : 'ru',
        themeColor: '#000',
        package   : 'ключ перезаписывается значениями из package.json wepster-сборщика',
        settings  : 'ключ перезаписывается значениями из файла настроек marmelad.settings.js',
        buildTime : '',
        icons     : [
            "menu",
            "chevron-left",
            "chevron-down",
            "chevron-up",
            "chevron-right",
            "close",
            "more_horiz",
            "more_vert",
            "favorite",
            "favorite_border"
        ]
    },
    theme    : {
        version    : '',
        name       : '',
        uri        : '',
        tags       : '',
        textdomain : '',
        description: '',
        message    : '',
        author     : {
            name: '',
            uri : ''
        },
        license    : {
            name: '',
            uri : ''
        },
        template   : ['/*',
            'Theme Name: <%= theme.name %>',
            'Theme URI: <%= theme.uri %>',
            'Author: <%= theme.author.name %>',
            'Author URI: <%= theme.author.uri %>',
            'Description: <%= theme.description %>',
            'Version: <%= theme.version %>',
            'License: <%= theme.license.name %>',
            'License URI: <%= theme.license.uri %>',
            'Tags: <%= theme.tags %>',
            'Text Domain: <%= theme.textdomain %>',
            '',
            '<%= theme.message %>',
            '*/',
            ''
        ].join('\n')
    },
    pageTitle: 'webster'
};
