'use strict';

let path = require('path');

let folders = {
    dist    : 'static',
    storage : '',
    wepster : 'wepster',
    _blocks : '_blocks',
    _helpers: '_helpers',
    _pages  : '_pages',
    iconizer: {
        src  : 'iconizer',
        icons: 'icons'
    },
    js      : {
        src    : 'js',
        vendors: 'vendors',
        plugins: 'plugins'
    },
    stylus  : 'stylus',
    static  : 'static'
};

let paths = {
    dist    : path.join(folders.dist),
    storage : path.join(folders.dist, folders.storage),
    wepster : path.join(folders.wepster),
    _blocks : path.join(folders.wepster, folders._blocks),
    _pages  : path.join(folders.wepster, folders._pages),
    _helpers: path.join(folders.wepster, folders._helpers),
    iconizer: {
        src  : path.join(folders.wepster, folders.iconizer.src),
        icons: path.join(folders.wepster, folders.iconizer.src, folders.iconizer.icons),
    },
    js      : {
        src    : path.join(folders.wepster, folders.js.src),
        vendors: path.join(folders.wepster, folders.js.src, folders.js.vendors),
        plugins: path.join(folders.wepster, folders.js.src, folders.js.plugins),
    },
    stylus  : path.join(folders.wepster, folders.stylus),
    static  : path.join(folders.wepster, folders.static),
};

let app = {

    beml        : {
        elemPrefix: '__',
        modPrefix : '_',
        modDlmtr  : '-'
    },
    autoprefixer: {
        browsers: [
            "last 2 version",
            "ie >= 10",
            "Android >= 4",
            "iOS >= 6",
            "Safari >= 6"
        ]
    },
    eslint      : {},
    babel       : {
        presets: ['es2015'],
        plugins: ["transform-object-assign"]
    },
    bsWP        : {
        ui            : false,
        ghostMode     : false,
        notify        : true,
        logLevel      : 'info',
        logPrefix     : 'WEPSTER PROXY',
        logFileChanges: true,
        proxy         : "http://localhost:3000",
        serveStatic   : ["./"],
        files         : './app.css',
    },
    bsSP        : {
        server        : {
            baseDir: paths.dist
        },
        port          : 8780,
        open          : false,
        directory     : true,
        ghostMode     : false,
        notify        : true,
        logLevel      : 'info',
        logPrefix     : 'WEPSTER STATIC',
        logFileChanges: false,
        ui            : false
    },
    svgSprite   : {
        mode: {
            symbol: { // symbol mode to build the SVG
                dest   : paths.iconizer.src, // destination folder
                sprite : 'sprite.svg', //sprite name
                example: false // Build sample page
            }
        },
        svg : {
            xmlDeclaration    : false, // strip out the XML attribute
            doctypeDeclaration: false // don't include the !DOCTYPE declaration
        }
    }
};

module.exports = {
    folders: folders,
    app    : app,
    paths  : paths
};
